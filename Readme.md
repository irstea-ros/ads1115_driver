# Launch the nodes

`cd ads1115_driver`

`docker-compose up -d`

# Look values

## Enter the container

`docker exec -it ads1115_driver_ads1115_1 bash`

## Echo the rostopic

`source /opt/ros/kinetic/setup.bash`

`rostopic list`

`rostopic echo /chatter`

# Plot value on other computer

`export ROS_MASTER_URI=http://rasp-abcde.local:11311`

`rqt_plot`

# Stop the nodes

`docker-compose stop`
