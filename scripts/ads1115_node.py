#!/usr/bin/env python
# Copyright 2018 Irstea
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rospy

from std_msgs.msg import Float32

import ADS1115 as ads1115_lib


class ADS1115(object):
    def __init__(self, channel):
        self.adc = ads1115_lib.ADS1115()

        # Anonymous topic ?
        self.pub = rospy.Publisher('chatter', Float32, queue_size=10)

        timer_period = 0.01  # in second
        self.timer = rospy.Timer(rospy.Duration(timer_period), self.timer_callback)
        rospy.spin()

    def timer_callback(self, event):
        msg = Float32()
        msg.data = self.adc.readADCSingleEnded()
        rospy.loginfo('Publishing: "{0}"'.format(msg.data))
        self.pub.publish(msg)


def main(args=None):
    rospy.init_node('ads1115', anonymous=True)

    node = ADS1115(1)


if __name__ == '__main__':
    main()
