FROM ros:kinetic-ros-core
# install ros tutorials packages
RUN apt-get update && apt-get install -y \
    ros-kinetic-rosbridge-suite python-pip \
    && rm -rf /var/lib/apt/lists/

RUN pip install ADS1115

COPY . /app
